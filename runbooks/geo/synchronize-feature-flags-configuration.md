# Get feature flags configuration as close to production environment as possible

Sometimes you can have hard times trying to reproduce production errors. One of the possible reasons
for this environment discrepancy can be a feature flags configuration. To configure local feature flags in the same way as it's on production you need to:

1. Delete everything from `feature_gates` table
  * Enter the local database console with `bundle exec rails dbconsole`
  * Run SQL: `DELETE FROM feature_gates;`
1. Generate CSV snapshot of the `feature_gates` table the GPRD console

```
COPY feature_gates TO STDOUT DELIMITER ',' CSV HEADER;
```

 Copy the output to the local file `/tmp/input`

1. Import the CSV snapshot to your table

```
COPY feature_gates FROM '/tmp/input' DELIMITER ',' CSV HEADER;
```

1. Clear the cache in your local primary and secondary nodes

```
redis-cli -s gdk-geo/redis/redis.socket flushall
redis-cli -s gdk-ee/redis/redis.socket flushall
```
